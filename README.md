# MU-POPBL2 

This repository holds the project called **Colorize** developed for the second semester POPBL of the [Data analysis, Cybersecurity and Cloud computing](https://www.mondragon.edu/en/master-degree-data-analysis-cybersecurity-cloud-computing) master degree at Mondragon University.

## Project Information

### Pipelines status

**Master branch**

![](https://gitlab.com/ivan.valdesi/mu-popbl2/badges/master/pipeline.svg)

**Staging branch**

![](https://gitlab.com/ivan.valdesi/mu-popbl2/badges/staging/pipeline.svg)


**Dev branch**

![](https://gitlab.com/ivan.valdesi/mu-popbl2/badges/dev/pipeline.svg)

### Coverage


![](https://gitlab.com/ivan.valdesi/mu-popbl2/badges/dev/coverage.svg)

## Getting started 

Prerequesites:  

- docker 
- docker-compose

The web application can be launch just runing the following command inside the web-app folder
   
    docker-compose up -d 

For more information read the docs in [doc.md](doc.md)

The servers are currently down to save gcloud billing. Contact with us to get the deployed app ip address!

## Changelog

Release v2.0.0 is now avaiable!

- Latest trained model version deployed.  
- Arround 70% of coverage. 
- Add a captcha on the images upload
- Final changes for delivery. 
  
## Docs

The details of the specifications, design and evidence of the Agile methodology followed are collected in the complementary documentation [doc.md](doc.md).

A useful quickstart guideline for the web application is available in the complementary documentation [README.md](web-app/README.md).

## Authors

- Mikel Amuchastegui [mikel.amuchastegui@alumni.mondragon.edu](mailto:mikel.amuchastegui@alumni.mondragon.edu)

- Iker Ocio [iker.ocio@alumni.mondragon.edu ](mailto:iker.ocio@alumni.mondragon.edu )

- Alexander Tesouro [alexander.tesouro@alumni.mondragon.edu](mailto:alexander.tesouro@alumni.mondragon.edu)
  
- Iván Valdés Irigoyen [ivan.valdesi@mondragon.edu](mailto:ivan.valdesi@mondragon.edu)

- Mikele Zurutuza [mikele.zurutuza@alumni.mondragon.edu](mailto:mikele.zurutuza@alumni.mondragon.edu)

##  License
This project is licensed under the [MIT License](https://choosealicense.com/licenses/mit/),  see the `LICENSE.md` file for details.