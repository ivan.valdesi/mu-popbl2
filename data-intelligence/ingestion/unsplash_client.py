import logging
import time

from urllib.request import urlretrieve
from pyunsplash import PyUnsplash
from downloader.downloader import download_from_url, get_image_bytes
from kafka.kafka_producer import produce

from config.config import Config

config = Config()

api_key = config.UNSPLASH_API_KEY
keyword = config.KEYWORD
topic = config.UNSPLASH_KAFKA_TOPIC

pu = PyUnsplash(api_key=api_key)
search = pu.search(type_="photos", query=keyword)

for i, photo in enumerate(search.entries):
    url = photo.link_download
    image = download_from_url(url)
    value = get_image_bytes(image)
    key = photo.id
    produce(topic, key, value)
    time.sleep(5)

    if i > 50:
        break