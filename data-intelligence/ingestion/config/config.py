import os

from dotenv import load_dotenv
load_dotenv()

class Config:
    KAFKA_HOST = os.getenv("KAFKA_HOST")
    KAFKA_PORT = os.getenv("KAFKA_PORT")
    FLICKR_API_KEY = os.getenv("FLICKR_API_KEY")
    FLICKR_API_SECRET = os.getenv("FLICKR_API_SECRET")
    KEYWORD = os.getenv("KEYWORD")
    UNSPLASH_API_KEY = os.getenv("UNSPLASH_API_KEY")
    FLICKR_KAFKA_TOPIC = os.getenv("FLICKR_KAFKA_TOPIC")
    UNSPLASH_KAFKA_TOPIC = os.getenv("UNSPLASH_KAFKA_TOPIC")