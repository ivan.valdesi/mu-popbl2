from urllib.request import urlretrieve
from io import BytesIO
from PIL import Image


def download_from_url(url):
    urlretrieve(url, 'tmp.png')
    image = Image.open('tmp.png') 
    image = image.resize((256, 256), Image.ANTIALIAS)
    return image

def get_image_bytes(image):
    imgByteArr = BytesIO()
    image.save(imgByteArr,format='PNG')
    return  imgByteArr.getvalue()