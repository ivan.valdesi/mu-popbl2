import flickrapi
import re
import time

from downloader.downloader import download_from_url, get_image_bytes
from kafka.kafka_producer import produce

from config.config import Config

config = Config()

api_key = config.FLICKR_API_KEY
api_secret = config.FLICKR_API_SECRET
keyword = config.KEYWORD
topic = config.FLICKR_KAFKA_TOPIC

flickr = flickrapi.FlickrAPI(api_key, api_secret)
photos = flickr.walk(text=keyword,
                     tag_mode='all',
                     tags=keyword,
                     extras='url_s',
                     per_page=100,       
                     sort='relevance')

for i, photo in enumerate(photos):

    url = photo.get('url_s')
    image = download_from_url(url)  
    value = get_image_bytes(image)
    key = re.search("https://live.staticflickr.com/.*/(.*.jpg)", url)
    key = key.group(1)
    produce(topic, key, value)
    time.sleep(5)
    
    if i > 50:
        break


