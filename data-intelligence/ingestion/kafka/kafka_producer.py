import os

from confluent_kafka import Producer
from config.config import Config

config = Config()

conf = {
    'bootstrap.servers': config.KAFKA_HOST + ':' + config.KAFKA_PORT,
    'security.protocol': 'SSL',
    'ssl.ca.location': 'kafka/colorize.crt'
}

p = Producer(conf)

def produce(topic, key, value):
    p.produce(topic, key=key, value=value)
    print("Sent!")
    p.flush()