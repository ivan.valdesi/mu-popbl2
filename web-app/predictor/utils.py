import base64
from io import BytesIO

from PIL import Image


def decode(base64_string):
    return Image.open(BytesIO(base64.b64decode(base64_string)))


def encode(image):
    imgByteArr = BytesIO()
    image.save(imgByteArr, format="PNG")
    base64_str = base64.b64encode(imgByteArr.getvalue()).decode("utf-8")
    # base64_str = base64.encodebytes(imgByteArr.getvalue()).decode('utf-8')
    return base64_str
