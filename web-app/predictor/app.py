import base64
import json
import os
from time import sleep

from PIL import Image

from consumer import Consumer
from healthcheck import Healthchecker
from predictor import Predictor
from publisher import Publisher
from structlog import PrintLogger, get_logger, wrap_logger
from structlog.processors import JSONRenderer
from utils import decode, encode

host = os.environ.get("RABBITMQ_IP")
port = 5671

prediction_exchange = "prediction_exchange"
results_exchange = "results_exchange"

print("Starting rabbitmq...")
p = Publisher(host, port, results_exchange, "fanout", "")
print("Rabbitmq started")
global index

index = 10

predictor = Predictor()


if os.getenv("STAGING") == "True":
    STAGING = True
else:
    STAGING = False
    h = Healthchecker("predictor")
    es = h.get_es()


def callback(ch, method, properties, body):
    global index
    print("Message arrived")
    new_json = json.loads(body)
    index += 20
    # lo convierto a color
    b64_image = new_json["picture_array_black"]
    image_format = new_json['filename'].split('.')[1]
    image = decode(b64_image)
    colorize_image_array = predictor.predict(image, image_format)
    colorize_image = Image.fromarray(colorize_image_array)
    # colorize_image = image.convert("1")
    b64_colorize_image = encode(colorize_image)

    new_json["picture_array_color"] = b64_colorize_image

    p.send_data(json.dumps(new_json), results_exchange, "")


c = Consumer(host, port, prediction_exchange, "fanout", "", callback)
