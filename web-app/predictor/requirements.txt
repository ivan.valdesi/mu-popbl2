pika==1.1.0
Pillow==7.1.2
pytest==5.4.2
pytest-cov==2.8.1
scikit-image==0.17.2
tqdm==4.46.0
#tensorflow==2.2.0
structlog==20.1.0
