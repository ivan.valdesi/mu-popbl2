import ssl
from time import sleep

import pika
from pika import exceptions


class Publisher:
    def __init__(self, host, port, exchange, exchange_type, routing_key):
        self.host = host
        self.port = port
        self.type = exchange_type
        self.routing_key = routing_key
        self.exchange = exchange
        self.open_connection(self.host, self.port, self.routing_key)

    def open_connection(self, host, port, routing_key):
        while True:
            try:
                context = ssl.create_default_context(cafile="/ca/aas_ca.crt")
                context.load_cert_chain(
                    "/ca/aas_rabbitmq_rbt.crt", "/ca/aas_rabbitmq_rbt.key"
                )
                ssl_options = pika.SSLOptions(context, "rabbitmq")

                self.connection = pika.BlockingConnection(
                    pika.ConnectionParameters(
                        host=host, port=port, ssl_options=ssl_options,
                    )
                )
            except exceptions.AMQPConnectionError:
                sleep(5)
                continue

            print("Connection stablished")
            break

        self.channel = self.connection.channel()
        self.channel.exchange_declare(
            exchange=self.exchange, exchange_type=self.type, durable=True
        )

    def send_data(self, data, exchange, routing_key):
        data = str(data)
        try:
            self.channel.basic_publish(
                exchange=self.exchange,
                routing_key=routing_key,
                body=data,
                properties=pika.BasicProperties(
                    delivery_mode=2,  # make message persistent
                ),
            )

            # print("[x] Sent " + data)
        except exceptions.AMQPConnectionError:
            self.open_connection(self.host, self.port, routing_key)
            self.send_data(data, exchange, routing_key)

    def close(self):
        self.connection.close()
