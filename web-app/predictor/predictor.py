import os
from copy import deepcopy
from glob import glob

import numpy as np
from PIL import Image

import cv2
import matplotlib.pyplot as plt
import tensorflow as tf
from skimage import color
from tensorflow.keras.layers import (
    Activation,
    AveragePooling2D,
    BatchNormalization,
    Concatenate,
    Conv2D,
    Conv2DTranspose,
    Dense,
    Dropout,
    Flatten,
    Input,
    LeakyReLU,
    MaxPooling2D,
    UpSampling2D,
)
from tensorflow.keras.models import Model, Sequential, load_model
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.preprocessing import image
from tensorflow.keras.preprocessing.image import array_to_img, img_to_array
import tensorflow as tf
from tqdm import tqdm

# os.environ["CUDA_VISIBLE_DEVICES"] = "-1"

if tf.test.gpu_device_name():
    print("GPU found")
else:
    print("No GPU found")


class Predictor:
    def __init__(self):
        self.generator = load_model("jupyter/generator_100_v5.h5")
        self.X_train = []

    def process(self, img):
        self.X_train = []
        img.save("jupyter/tmp.jpg")

        img = cv2.imread("jupyter/tmp.jpg")

        resized = np.array(cv2.resize(img, (128, 128), interpolation=cv2.INTER_AREA))

        self.X_train.append(resized)
        X_train = np.array(self.X_train)
        X_train.shape

        image = X_train[0]

        L = np.array(self.rgb_to_lab(image, lchannel=True))

        X_train_L = L

        X_test_L = X_train_L
        return X_test_L
    
    def process2(self, img, image_format):
        self.X_train = []
        img.save("jupyter/tmp.{}".format(image_format))

        input_image = tf.keras.preprocessing.image.load_img("jupyter/tmp.{}".format(image_format), target_size=(128, 128))
        input_image = tf.keras.preprocessing.image.img_to_array(input_image)
        input_image = np.array(input_image, dtype=float)
        
        input_image = input_image.astype('uint8')
        input_image = color.rgb2lab(input_image*1.0/255)
        input_image = np.array(input_image)

        X_test = deepcopy(input_image[:,:,0])
        X_test = X_test.reshape(1, 128, 128, 1)
        return X_test

    def predict(self, img, image_format):
        # img = cv2.imread(f)
        print(img)
        X_test = self.process2(img, image_format)

        pred = self.generator.predict(X_test.reshape(1, 128, 128, 1))
        print(pred.shape)
        img = self.postprocess2(pred, X_test)
        return img
        # plt.imshow(img)
        # plt.show()
    
    def postprocess(self, pred, X_test):
        # no  va
        pred_reshap = pred.reshape(128, 128, 2)
        img2 = np.dstack((X_test, pred_reshap))
        return self.lab_to_rgb(img2)
    
    def postprocess2(self, pred, X_test):
        pred *= 128
        cur = np.zeros((128, 128, 3))
        cur[:,:,0] = X_test[0][:,:,0]
        cur[:,:,1:] = pred[0]
        xyz = color.lab2xyz(cur)
        rgb = color.xyz2rgb(xyz)
        
        rgb *= 255
        return np.uint8(rgb) # array

    def rgb_to_lab(self, img, lchannel=False, abchannel=False):
        img = img / 255
        l = color.rgb2lab(img)[:, :, 0]
        l = l / 50 - 1
        l = l[..., np.newaxis]

        ab = color.rgb2lab(img)[:, :, 1:]
        ab = (ab + 128) / 255 * 2 - 1
        if lchannel:
            return l
        else:
            return ab

    # Input L or AB channels in range -1 to 1
    # Outputs RGB channels in range 0-255
    # Denormalize

    def lab_to_rgb(self, img):
        new_img = np.zeros((128, 128, 3))
        for i in range(len(img)):
            for j in range(len(img[i])):
                pix = img[i, j]
                new_img[i, j] = [
                    (pix[0] + 1) * 50,
                    (pix[1] + 1) / 2 * 255 - 128,
                    (pix[2] + 1) / 2 * 255 - 128,
                ]
        new_img = color.lab2rgb(new_img) * 255
        new_img = new_img.astype("uint8")
        return new_img
