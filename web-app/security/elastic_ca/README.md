## Info

Certificados de CA -> CA/colorize.crt, CA/colorize.key
Certificados Elastic -> elastic-client.crt, elastic-client.key

## Clave para conectarse a EC2
pobl2.pem



## Generar elastic certs

Al generar el certificado es muy importante poner el IP SANs del server de elastic bien

```sh
openssl genrsa -out elastic.key 2048
openssl req -new -key elastic.key -out elastic.csr
# firma por el ca
openssl x509 -req -in elastic.csr -CA colorize.crt -CAkey colorize.key -CAcreateserial -extfile openssl.conf -out elastic.crt

```

