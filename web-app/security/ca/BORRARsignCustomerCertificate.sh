if [ "$1" ]; then
	echo "CA pem password is: 1234";
	openssl ca -in $1.csr -out $1.crt -cert ca.crt -keyfile ca.key -config openssl.cnf
else
	echo "\nAn argument is needed"
	echo "./signCustomerCertificate.sh aas_auth_ssl"
fi
