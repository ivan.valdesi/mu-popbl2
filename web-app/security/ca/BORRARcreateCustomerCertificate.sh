if [ "$1" ]; then
	openssl genrsa -aes128 -out $1.key 4096
	openssl req -new -key $1.key -out $1.csr
else
	echo "\nPlease one argument as a name is needed"
	echo "./createCustomerCertificate.sh aas_auth_ssl\n"
fi
