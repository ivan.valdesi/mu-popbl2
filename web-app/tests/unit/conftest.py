import pytest

from project import create_app, db
from project.web import models

PRESERVE_CONTEXT_ON_EXCEPTION = False

from PIL import Image
from io import BytesIO

# esto habria que hacerlo en un session o con un if dependiendo del tipo de test
@pytest.fixture(scope="session", autouse=True)
def test_debug():
    import os

    test_type = os.getenv("REMOTE_DEBUG")
    if test_type == "True":
        import ptvsd

        ptvsd.enable_attach(address=("0.0.0.0", 14000))
        ptvsd.wait_for_attach()


@pytest.fixture(scope="session")
def seed_database(test_app):
    import json

    client = test_app.test_client()
    client.post(
        "/order",
        data=json.dumps(
            {
                "client_id": "admin",
                "number_of_pieces": 4,
                "type": "type_a",
                "address": "01",
            }
        ),
        content_type="application/json",
    )


@pytest.fixture(scope="function")
def reset_database():
    models.Base.metadata.drop_all(db.engine)
    models.Base.metadata.create_all(db.engine)


@pytest.fixture(scope="session")
def test_app():
    app = create_app("project.config.DevelopmentConfig")
    with app.app_context():
        yield app


@pytest.fixture(scope="function")
def test_app_with_csrf():
    app = create_app("project.config.ProductionConfig")
    with app.app_context():
        with app.test_request_context():
            yield app


@pytest.fixture(scope="session")
def testing_image():
    img = Image.new("RGB", (25, 25), color=(120, 120, 20))
    byte_io = BytesIO()
    img.save(byte_io, "png")
    byte_io.seek(0)
    return byte_io
