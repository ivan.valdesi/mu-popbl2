from flask_wtf.csrf import generate_csrf
from project.web.models.user import User


def test_auth_invalid_user(test_app):
    client = test_app.test_client()
    resp = client.post(
        "/admin/login",
        data={"username": "iker", "password": "ocio", "csrf_token": "test"},
        content_type="application/x-www-form-urlencoded",
    )
    assert resp.status_code == 302


def test_auth_with_csrf(test_app_with_csrf):
    client = test_app_with_csrf.test_client()
    resp = client.post(
        "/admin/login",
        data={"username": "iker", "password": "ocio",},
        content_type="application/x-www-form-urlencoded",
    )
    content = resp.data.decode("utf-8")
    assert "The CSRF token is missing" in content
    assert resp.status_code == 400


def test_admin_correct_login(test_app):
    client = test_app.test_client()
    resp = client.post(
        "admin/login",
        data={"username": "admin", "password": "admin", "csrf_token": "test"},
        content_type="application/x-www-form-urlencoded",
    )
    assert resp.status_code == 200
    resp = client.get("admin/logout", content_type="application/x-www-form-urlencoded",)
    assert resp.status_code == 302


def test_admin_invalid_password(test_app):
    client = test_app.test_client()
    resp = client.post(
        "admin/login",
        data={"username": "admin", "password": "invalid_pass", "csrf_token": "test"},
        content_type="application/x-www-form-urlencoded",
    )
    assert resp.status_code == 302
    resp = client.get("admin/logout", content_type="application/x-www-form-urlencoded",)
    assert resp.status_code == 302


def test_normal_user_login(test_app):
    client = test_app.test_client()
    resp = client.post(
        "admin/login",
        data={"username": "user", "password": "user", "csrf_token": "test"},
        content_type="application/x-www-form-urlencoded",
    )
    assert resp.status_code == 200
    resp = client.get("user/", content_type="application/x-www-form-urlencoded")
    assert resp.status_code == 200
    resp = client.get("admin/logout", content_type="application/x-www-form-urlencoded",)
    assert resp.status_code == 302
