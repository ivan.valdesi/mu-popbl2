import json

import pytest

from flask import session
from project.web.services import captcha_generator


# 5ec6cf2976d0ec383cecbde1
@pytest.mark.parametrize(
    "object_id", ["123456789012345678901234", "5ec6cf2976d0ec383cecbde1"]
)
def test_picture(test_app, object_id):
    client = test_app.test_client()
    resp = client.post(
        "/pictureIsReady/" + object_id,
        content_type="application/x-www-form-urlencoded",
    )
    assert resp.status_code == 200


def test_invalid_picture(test_app):
    client = test_app.test_client()
    resp = client.post(
        "/pictureIsReady/" + "5ec6cf2976d0ec383cecbde1",
        content_type="application/x-www-form-urlencoded",
    )

    content = json.loads(resp.data)
    if content["is_ready"] is False:
        assert (content["error"] == "Invalid Object ID value") or (
            content["error"] == "object does not exists"
        )


def test_valid_picture_not_exists(test_app):
    client = test_app.test_client()
    resp = client.post(
        "/pictureIsReady/" + "5ec6cf2976d0ec383cecbde1",
        content_type="application/x-www-form-urlencoded",
    )

    content = json.loads(resp.data)
    if content["is_ready"] is False:
        assert content["error"] == "object does not exists"


def test_404(test_app):
    client = test_app.test_client()
    resp = client.post(
        "/adasdasdasd", content_type="application/x-www-form-urlencoded",
    )
    assert resp.status_code == 404


def test_403(test_app):
    client = test_app.test_client()
    resp = client.get("/admin", content_type="application/x-www-form-urlencoded",)
    assert resp.status_code == 308


def test_index(test_app):
    client = test_app.test_client()
    resp = client.post("/", content_type="application/x-www-form-urlencoded",)
    assert resp.status_code == 200


def test_terms_no_accept(test_app):
    client = test_app.test_client()
    client.post(
        "/terms/0", content_type="application/x-www-form-urlencoded",
    )
    with client.session_transaction() as session:
        assert session["acceptance_terms"] == False


def test_terms_accept(test_app):
    client = test_app.test_client()
    client.post(
        "/terms/1", content_type="application/x-www-form-urlencoded",
    )
    with client.session_transaction() as session:
        assert session["acceptance_terms"] == True


def test_upload_empty(test_app):
    client = test_app.test_client()

    resp = client.post("/upload", content_type="application/x-www-form-urlencoded",)
    json_content = json.loads(resp.data)
    assert json_content["success"] == False


def test_upload_correct_photo(test_app, testing_image, monkeypatch):
    def mockreturn(uuid, solved):
        print("Patched")
        return True

    from project.web.services import captcha_generator

    monkeypatch.setattr(captcha_generator, "check_captcha", mockreturn)

    client = test_app.test_client()

    data = {}
    data["file"] = (testing_image, "test.png")
    data["captcha"] = "not valid"
    data["captcha_uuid"] = "not valid"
    resp = client.post("/upload", content_type="multipart/form-data", data=data)
    assert resp.status_code == 200
