# MU-POPBL2 Web App

This document holds a quick guideline for the Web application developed for the second semester POPBL of the [Data Analysis, Cybersecurity and Cloud Computing](https://www.mondragon.edu/es/master-universitario-analisis-datos-ciberseguridad-computacion-nube) master degree at Mondragon University.

## How to debug testing

1. Prerequisites:

- Activate environment variable REMOTE_DEBUG=True
- Set a breakpoint

2. Change entrypoint:

```sh
entrypoint: ["python3", "manage.py", "run", "-h", "0.0.0.0", "-p", "5000", "--cert=/conf/ca/aas_web_app_ssl.crt", "--key=/conf/ca/aas_web_app_ssl.key"]
```

3. Execute test.

4. The process will maintain waiting until debug is connected.

## How to debug app and test

1. Change entrypoint to start with VST.

```sh
entrypoint: ["python3", "-m", "ptvsd", "--host", "0.0.0.0", "--port", "14000", "manage.py", "run", "--no-reload", "-h", "0.0.0.0"] 
```
2. Set a breakpoint.

3. Start application:

```sh
docker-compose up
```

4. Connect to VSCode.

## Useful commands

```sh
docker exec flask_app python3 -m pytest 'tests' -p no:warnings --cov=project --cov-fail-under=50
docker exec flask_app python3 -m black ./
docker exec flask_app python3 -m flake8 ./project
docker exec flask_app /bin/sh -c "isort */*.py"
```

## Application inputs

- Image inserted by user in the home page.
- Admin login credentials.
