import json
import logging
import os
from datetime import datetime

from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_toastr import Toastr
from flask_wtf.csrf import CSRFProtect

from project.healthcheck import Healthchecker
from project.web.services.mongo_service import update_picture
from project.web.services.kafka_service import produce

logging.basicConfig()

csrf = CSRFProtect()

db = SQLAlchemy()

APP_VERSION = os.getenv("APP_VERSION")

toastr = Toastr()

if os.getenv("STAGING") == "True":
    STAGING = True
    elastic = None
else:
    STAGING = False
    h = Healthchecker("flask_app")
    elastic = h.get_es()


if not os.path.exists("project/static/uploads/black"):
    os.makedirs("project/static/uploads/black")

if not os.path.exists("project/static/uploads/color"):
    os.makedirs("project/static/uploads/color")

if not os.path.exists("project/static/uploads/tmp"):
    os.makedirs("project/static/uploads/tmp")


def create_app(test_config=None):
    # create and configure the app
    app = Flask(__name__, instance_relative_config=True, template_folder="templates")
    if test_config is None:
        app_settings = os.getenv("APP_SETTINGS")
    else:
        app_settings = test_config
    app.config.from_object(app_settings)
    print(
        "FILE MAX CONTENT: {} mb".format(
            (app.config["MAX_CONTENT_LENGTH"] / (1024 * 1024))
        )
    )
    toastr.init_app(app)

    csrf.init_app(app)

    db.init_app(app)

    @app.shell_context_processor
    def ctx():
        return {"app": app, "db": db}

    @app.context_processor
    def inject_now():
        return {"now": datetime.utcnow()}

    from project.web.routes import admin, home, user

    app.register_blueprint(admin.bp)
    app.register_blueprint(home.bp)
    app.register_blueprint(user.bp)

    from project.web.routes.errors import page_not_found

    app.register_error_handler(404, page_not_found)
    # app.register_error_handler(413, request_too_large)

    host = os.environ.get("RABBITMQ_IP")
    port = 5671
    results_exchange = "results_exchange"

    # with app.app_context():
    from project.web.services.mqtt_consumer import Consumer

    def mqtt_callback(ch, method, properties, body):
        json_obj = json.loads(body)
        json_obj["filename"] = json_obj["filename"].replace("black", "color")
        produce("predictions", json_obj)
        update_picture(json_obj)
        # save in mongodb new image
        # save in folder the image

    Consumer(host, port, results_exchange, "fanout", "", mqtt_callback, app)

    return app
