# project/config.py
import os


class BaseConfig:
    TESTING = False
    SQLALCHEMY_TRACK_MODIFICATIONS = False  # new
    SECRET_KEY = "my_precious"  # new
    UPLOAD_FOLDER = "project/static/uploads"
    MAX_CONTENT_LENGTH = 1 * 1024 * 1024


class DevelopmentConfig(BaseConfig):
    SQLALCHEMY_DATABASE_URI = os.environ.get("SQLALCHEMY_DATABASE_URI")  #
    WTF_CSRF_ENABLED = False
    print("WTF_CSRF disabled for development environment")


class ProductionConfig(BaseConfig):
    SQLALCHEMY_DATABASE_URI = os.environ.get("SQLALCHEMY_DATABASE_URI")  #
