from flask import (
    Blueprint,
    flash,
    jsonify,
    redirect,
    render_template,
    request,
    session,
    url_for,
)
import logging
import os
from project import APP_VERSION, STAGING
from project.web.models.user import User
from project.web.services.file_pipeline import pipeline
from project.web.services.mongo_service import get_picture
from project.web.services.captcha_generator import generate_captcha, check_captcha

bp = Blueprint("home", __name__, url_prefix="/")

ALLOWED_WHITELIST_EXTENSIONS = ["png", "jpg", "jpeg"]

logging.basicConfig(filename="/logging/home.log", level=logging.DEBUG)


@bp.route("/<string:m_type>/<string:message>", methods=("GET", "POST"))
@bp.route("/", methods=("GET", "POST"), defaults={"m_type": None, "message": None})
def index(m_type, message):
    if m_type is not None and message is not None:
        flash(message, m_type)

    terms_accepted = session.get("acceptance_terms")
    if terms_accepted is None or not terms_accepted:
        return render_template("terms.html", app_name="colorize")
    else:
        print("Terms accepted")

    user_id = session.get("user_id")
    if user_id is not None:
        user = User.query.filter_by(id=user_id).first()
    else:
        user = None

    c_uuid, c_src = generate_captcha()
    return render_template(
        "index.html",
        user=user,
        version=APP_VERSION,
        staging=STAGING,
        captcha_uuid=c_uuid,
        captcha_src=c_src,
    )


@bp.route("/terms/<string:acceptance>", methods=("GET", "POST"))
def terms(acceptance):
    print(acceptance)
    if acceptance == "1":
        session["acceptance_terms"] = True
        print("A")
    else:
        session["acceptance_terms"] = False
        print("B")

    return redirect(url_for("home.index"))


@bp.route("/pictureIsReady/<string:object_id>", methods=("GET", "POST"))
def picture_is_ready(object_id):
    print(object_id)
    picture_json = get_picture(object_id)
    if picture_json is None:
        return jsonify({"is_ready": False, "error": "object does not exists"})
    if "error" in picture_json:
        return jsonify(
            {"is_ready": False, "error": True, "object": picture_json["error"]}
        )
        return picture_json
    if "picture_array_color" in picture_json:
        return jsonify(
            {
                "is_ready": True,
                "filename_black": "/static/uploads/black/{}.{}".format(
                    object_id, picture_json["file_format"]
                ),
                "filename_color": "/static/uploads/color/{}.{}".format(
                    object_id, picture_json["file_format"]
                ),
            }
        )
    else:
        return jsonify({"is_ready": False, "error": False})


def delete_captchas_images():
    folderpath = os.getcwd() + "/project/static/uploads/captchas"
    for filename in os.listdir(folderpath):
        if ".png" in filename:
            os.unlink(folderpath + "/" + filename)


@bp.route("upload", methods=("GET", "POST"))
def upload_file():
    def allowed_file(filename):
        return (
            (
                "." in filename
                and filename.rsplit(".", 1)[1].lower() in ALLOWED_WHITELIST_EXTENSIONS
            ),
            filename.rsplit(".", 1)[1].lower(),
        )

    if request.method == "POST":
        logging.debug("A")
        # check captcha

        if session.get('user_id') is None:
            captcha_solved = request.form.get("captcha", None)
            captcha_uuid = request.form.get("captcha_uuid", None)
            if not check_captcha(captcha_uuid, captcha_solved):
                return jsonify({"success": False, "error": "Captcha is not correct"})
            print(captcha_solved)
            print(captcha_uuid)

        # check if the post request has the file part
        if "file" not in request.files:
            flash("No file part")
            return redirect(request.url)
        file = request.files["file"]
        # if user does not select file, browser also
        # submit an empty part without filename
        if file.filename == "":
            flash("No selected file")
            return redirect(request.url)

        is_file_valid, file_format = allowed_file(file.filename)
        if file and is_file_valid:

            user_id = session.get("user_id")
            if user_id is not None:
                user = User.query.filter_by(id=user_id).first()
                username = user.username
            else:
                username = "unknown"
            service_result = pipeline(username, file)
            delete_captchas_images()
            if service_result.executed:
                response = {
                    "success": True,
                    "message": "image saved",
                    "service_result": service_result.to_dict(),
                }
            else:
                response = {"success": False, "message": service_result.error}

            return jsonify(response)
        else:
            return jsonify(
                {
                    "success": False,
                    "error": "Image {} format not supported. Need to be one of {}".format(
                        file_format, ALLOWED_WHITELIST_EXTENSIONS
                    ),
                }
            )

    flash("GET method is not supported")
