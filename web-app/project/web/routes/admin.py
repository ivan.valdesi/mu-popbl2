import functools

from flask import (
    Blueprint,
    flash,
    redirect,
    render_template,
    request,
    session,
    url_for,
    abort,
)
from cerberus import Validator
from project.web.models.user import User
import json
from project.web.services.mongo_service import get_all
from project.web.services.mongo_service import find_picture
from project.web.services.captcha_generator import generate_captcha

bp = Blueprint("admin", __name__, url_prefix="/admin")

admin_whitelist = ["admin"]


def login_required(view):
    @functools.wraps(view)
    def wrapped_view(**kwargs):
        user_id = session.get("user_id")
        if user_id is None:
            return render_template("403.html")
        user = User.query.filter_by(id=user_id).first()

        if user.role in admin_whitelist:
            return view(**kwargs)
        else:
            return render_template("403.html")

    return wrapped_view


@bp.route("/", methods=("GET", "POST"))
@login_required
def index():
    user_id = session.get("user_id")
    if user_id is not None:
        user = User.query.filter_by(id=user_id).first()
        pictures = get_all()

        c_uuid, c_src = generate_captcha()
        print("AAA")
        return render_template("admin.html", user=user, pictures=pictures)
    else:
        abort(403)


@bp.route("/login", methods=("GET", "POST"))
def login():

    schema = {
        "username": {
            "type": "string",
            "required": True,
            "minlength": 4,
            "maxlength": 10,
        },
        "password": {
            "type": "string",
            "required": True,
            "minlength": 4,
            "maxlength": 10,
        },
        "csrf_token": {"type": "string", "required": True},
    }
    v = Validator(schema)

    if v.validate(request.form.to_dict()) and request.method == "POST":
        username = request.form["username"]
        password = request.form["password"]
        error = None

        user = User.query.filter_by(username=username).first()

        if user is None:
            error = "Incorrect username."
        elif not user.check_password(password):
            error = "Incorrect password."

        if error is None:
            session.clear()
            session["user_id"] = user.id
            flash("Login successfully", "success")
            if user.role == "admin":
                pictures = get_all()
                return render_template("admin.html", user=user, pictures=pictures)
            else:
                pictures = find_picture(user.username)
                response_dict = {"user": user}
                if pictures is not None:
                    response_dict["pictures"] = pictures
                    c_uuid, c_src = generate_captcha()
                    response_dict["captcha_uuid"] = c_uuid
                    response_dict["captcha_src"] = c_src

                return render_template("user.html", **response_dict)

        flash(error)
    else:
        error = json.dumps(v.errors)
    return redirect(url_for("home.index", m_type="error", message=error))


@bp.route("/logout")
def logout():
    message = "Logout successfully"
    session.clear()
    return redirect(url_for("home.index", m_type="info", message=message))
