from flask import Blueprint, render_template, session
from flask import abort
from project.web.models.user import User
from project.web.services.mongo_service import find_picture
from project.web.services.captcha_generator import generate_captcha

bp = Blueprint("user", __name__, url_prefix="/user")


@bp.route("/", methods=("GET", "POST"))
def index():
    user_id = session.get("user_id")
    if user_id is not None:
        user = User.query.filter_by(id=user_id).first()
        pictures = find_picture(user.username)
        response_dict = {"user": user}
        if pictures is not None:
            response_dict["pictures"] = pictures

        c_uuid, c_src = generate_captcha()
        response_dict["captcha_uuid"] = c_uuid
        response_dict["captcha_src"] = c_src
        print("AAA")
        return render_template("user.html", **response_dict)
    else:
        abort(403)
