from project import db


class Captcha(db.Model):
    __tablename__ = "captchas"
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    captcha_uuid = db.Column(db.String(128), nullable=False)
    captcha_solved = db.Column(db.String(128))

    def __init__(self, uuid, solved):
        self.captcha_uuid = uuid
        self.captcha_solved = solved

    def save(self):
        if self.id is None:
            db.session.add(self)
        return db.session.commit()

    @staticmethod
    def check_captcha(uuid, solved_text):
        c = Captcha.query.filter_by(
            captcha_uuid=uuid, captcha_solved=solved_text
        ).first()
        print(c)
        return c is not None
