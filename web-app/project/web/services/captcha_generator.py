import random
import string
from PIL import Image
from claptcha import Claptcha
from ..models.captcha import Captcha
import uuid


def randomString():
    rndLetters = (random.choice(string.ascii_uppercase) for _ in range(6))
    return "".join(rndLetters)


def generate_captcha():
    # Initialize Claptcha object with random text, FreeMono as font, of size
    # 100x30px, using bicubic resampling filter and adding a bit of white noise
    c = Claptcha(
        randomString,
        "/usr/share/fonts/truetype/dejavu/DejaVuSansMono.ttf",
        (250, 75),
        resample=Image.BICUBIC,
        noise=0.1,
    )

    selected_uuid = str(uuid.uuid1())
    c_path = "/static/uploads/captchas/{}.png".format(selected_uuid)
    text, _ = c.write("/app/project/" + c_path)
    Captcha(uuid=selected_uuid, solved=text).save()

    print(text)
    return selected_uuid, c_path


def check_captcha(uuid, solved):
    return Captcha.check_captcha(uuid, solved)
