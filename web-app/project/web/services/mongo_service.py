import base64
import datetime
import json
import os
from io import BytesIO

from bson.objectid import ObjectId
from PIL import Image
from pymongo import MongoClient

uri = os.environ.get("MONGO_DATABASE_URI")
client = MongoClient(uri)

db = client.user_predictions


def decode(base64_string):
    return Image.open(BytesIO(base64.b64decode(base64_string)))


class JSONEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, ObjectId):
            return str(o)
        return json.JSONEncoder.default(self, o)


def insert_picture(user, picture, file_format):
    picture = {
        "author": user,
        "picture_array_black": picture,
        "date": datetime.datetime.utcnow(),
        "file_format": file_format,
    }
    print("Picture inserted to user: {}".format(user))
    picture_id = db.pictures.insert_one(picture).inserted_id

    return str(picture_id)


def update_picture(json_obj):
    picture_id, new_picture = json_obj["id"], json_obj["picture_array_color"]
    image = decode(new_picture)
    print(json_obj["filename"])
    image.save(json_obj["filename"])

    id_query = {"_id": ObjectId(picture_id)}
    set_query = {"$set": {"picture_array_color": new_picture}}
    db.pictures.update(id_query, set_query)


def find_picture(user):
    pictures = db.pictures
    user_pictures = list(pictures.find({"author": user}).sort('date', -1))
    print("{} has {} pictures".format(user, len(user_pictures)))
    for picture in user_pictures:
        picture["picture_array_black"] = "/static/uploads/black/{}.{}".format(
            picture["_id"], picture['file_format']
        )
        picture["picture_array_color"] = "/static/uploads/color/{}.{}".format(
            picture["_id"], picture['file_format']
        )
    return user_pictures


def get_picture(picture_id):
    pictures = db.pictures
    try:
        return pictures.find_one({"_id": ObjectId(picture_id)})
    except Exception as e:
        return {"is_ready": False, "error": e}


def get_all():
    images = []
    pictures = db.pictures
    for pic in pictures.find():
        black_url = "/static/uploads/black/{}.{}".format(pic["_id"], pic['file_format'])
        if "picture_array_color" in pic:
            color_url = "/static/uploads/color/{}.{}".format(pic["_id"], pic['file_format'])
            images.append(
                {
                    "datetime": pic["date"],
                    "picture_array_black": black_url,
                    "picture_array_color": color_url,
                }
            )
        else:
            images.append({"datetime": pic["date"], "picture_array_black": black_url})
    return images
