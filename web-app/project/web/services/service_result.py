class ServiceResult:
    executed = False
    error = ""
    obj = {}

    def __init__(self, executed, error=""):
        self.executed = executed
        self.error = error

    def set_object(self, obj):
        self.obj = obj

    def to_dict(self):
        return {"obj": self.obj}
