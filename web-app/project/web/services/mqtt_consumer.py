import ssl
import time
from threading import Thread

import pika
from pika import exceptions


class Consumer(Thread):  # Thread
    def __init__(self, host, port, exchange, exchange_type, routing_key, callback, app):
        Thread.__init__(self)
        self.daemon = True
        self.app = app
        while True:
            try:

                context = ssl.create_default_context(cafile="/ca/aas_ca.crt")
                context.load_cert_chain(
                    "/ca/aas_rabbitmq_rbt.crt", "/ca/aas_rabbitmq_rbt.key"
                )
                ssl_options = pika.SSLOptions(context, "rabbitmq")

                self.connection = pika.BlockingConnection(
                    pika.ConnectionParameters(
                        host=host, port=port, ssl_options=ssl_options,
                    )
                )
            except exceptions.AMQPConnectionError:
                print("Error connection to RabbitMQ server")
                time.sleep(5)
                continue

            break

        print("Connected")
        self.channel = self.connection.channel()
        self.type = exchange_type
        self.channel.exchange_declare(
            exchange=exchange, exchange_type=self.type, durable=True
        )
        result = self.channel.queue_declare(queue="", durable=True)
        self.channel.queue_bind(
            exchange=exchange, queue=result.method.queue, routing_key=routing_key
        )
        self.channel.basic_consume(
            queue=result.method.queue, auto_ack=False, on_message_callback=callback
        )

        self.start()
        # self.run()

    def run(self):
        print("[*] Waiting for messages. To exit press CTRL+C")
        with self.app.app_context():
            self.channel.start_consuming()
