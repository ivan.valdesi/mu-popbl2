import base64
import json
import os
from io import BytesIO

from flask import current_app
from PIL import Image
from werkzeug.utils import secure_filename

from .mongo_service import insert_picture
from .mqtt_publisher import Publisher
from .service_result import ServiceResult

MAX_SIZE = 31200

host = os.environ.get("RABBITMQ_IP")
port = 5671

prediction_exchange = "prediction_exchange"
results_exchange = "results_exchange"


# temp image saved in url_for("static", filename="uploads/" + file.filename)
# url_for("static", filename="uploads/" + file.filename)
def pipeline(username, file):
    # check filename
    filename = secure_filename(file.filename)
    file_format = filename.split(".")[-1]
    # save file in temp folder
    temp_filepath = save_in_temp_folder(file)
    print("Filepath: {}".format(temp_filepath))
    image = open_file(temp_filepath)
    if image is None:
        return ServiceResult(False, "File format is not valid")

    if image.size[0] > MAX_SIZE or image.size[1] > MAX_SIZE:
        return ServiceResult(False, "Size is too much")
    else:
        pass

    base64_str = encode(image)

    image_id = insert_picture(username, base64_str, file_format)

    black_filename = prepare_new_filename(filename, image_id)
    image.save(black_filename)

    p = Publisher(host, port, prediction_exchange, "fanout", "")
    p.send_data(
        json.dumps(
            {
                "id": image_id,
                "author": username,
                "picture_array_black": base64_str,
                "filename": black_filename,
            }
        ),
        results_exchange,
        "",
    )
    p.close()

    service_result = ServiceResult(executed=True)
    service_result.set_object(
        {
            "id": image_id,
            "filename_black": "/static/uploads/black/{}.{}".format(
                image_id, file_format
            ),
        }
    )
    return service_result


def save_in_temp_folder(file):
    filename = secure_filename(file.filename)
    upload_folder = current_app.config["UPLOAD_FOLDER"]
    temp_filepath = os.path.join(upload_folder, "tmp", filename)
    file.save(temp_filepath)
    return temp_filepath


def prepare_new_filename(filename, image_id):
    upload_folder = current_app.config["UPLOAD_FOLDER"]
    return os.path.join(
        upload_folder,
        "black",
        "{}.{}".format(image_id, filename.rsplit(".", 1)[1].lower()),
    )


def open_file(filepath):
    try:
        image = Image.open(filepath)
        image = image.resize((256, 256), Image.ANTIALIAS)
        return image
    except Exception as e:
        print(e)
        return None


def encode(image):
    imgByteArr = BytesIO()
    image.save(imgByteArr, format="PNG")
    base64_str = base64.b64encode(imgByteArr.getvalue()).decode("utf-8")
    # base64_str = base64.encodebytes(imgByteArr.getvalue()).decode('utf-8')
    return base64_str


def decode(base64_string):
    return Image.open(BytesIO(base64.b64decode(base64_string)))
