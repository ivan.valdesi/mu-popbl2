from confluent_kafka import Producer
from confluent_kafka import KafkaException

conf = {
    "bootstrap.servers": "3.210.78.17:9092",
    "security.protocol": "SSL",
    "ssl.ca.location": "/ca/colorize.crt",
}

p = Producer(conf)


def produce(topic, json_obj):
    try:
        print("Sending message to Kafka producer")
        p.produce(
            topic, key=json_obj["filename"], value=json_obj["picture_array_color"]
        )
        print("Sent message to Kafka producer!")
    except KafkaException as exception:
        print("An error ocurred while sending the message to Kafka Producer")
        print("Error: " + str(exception.args[0]))
