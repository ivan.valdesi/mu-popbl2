import os
from datetime import datetime
from ssl import create_default_context
from threading import Thread
from time import sleep

from elasticsearch import Elasticsearch


class Healthchecker(Thread):
    def __init__(self, filename):
        Thread.__init__(self)
        self.daemon = True
        self.filename = filename

        host = os.environ.get("ELASTIC_HOST")
        ssl_ca = "/ca/colorize.crt"
        # ssl_crt = "/ca/elastic-client.crt"
        # ssl_key = "/ca/elastic-client.key"
        user = os.environ.get("ELK_USER")
        passw = os.environ.get("ELK_PASSWORD")

        context = create_default_context(cafile=ssl_ca)
        context.check_hostname = False

        try:
            self.es = Elasticsearch(
                [host],
                scheme="https",
                port=9200,
                http_auth=(user, passw),
                ssl_context=context,
            )
            self.es.index(
                index="prueba-{}-index".format(self.filename),
                id=1,
                body={"type": "check"},
            )
        except Exception:
            self.es = None
            print("Elastic not connected.")
            print("Elastic is not going to be used in this session")

        self.start()

    def get_es(self):
        return self.es

    def run(self):
        id_es = 1
        while True:
            doc = {
                "app": self.filename,
                "alive": True,
                "timestamp": datetime.now(),
            }
            id_es += 1
            if self.es is not None:
                self.es.index(
                    index="log-{}-index".format(self.filename), id=id_es, body=doc
                )
            sleep(2.5)
            if id_es >= 1000:
                id_es = 1
