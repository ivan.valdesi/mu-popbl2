
Create postgres configmap

postgres

    kubectl create configmap popbl-postgres-config --from-file  ./project/db/create.sql 

mongo
    kubectl create configmap popbl-mongo-config --from-file  ./project/mongodb/init-mongo.js


ca

    kubectl create secret generic popbl-ca-secret --from-file ./security/ca



 gitalb credentials secret 

	kubectl create secret generic gitalb-credentials --from-file=.dockerconfigjson=/home/ivaldes/.docker/config.json  --type=kubernetes.io/dockerconfigjson


Create Cluster

        gcloud container clusters create production-cluster \
        --num-nodes 2 \
        --machine-type n1-standard-2 \
        --scopes "https://www.googleapis.com/auth/source.read_write,cloud-platform"


gcloud auth

                gcloud auth activate-service-account --key-file <file>
                                                                



kubectl create secret generic ingress-tls --from-file=tls.crt=security/ca/aas_web_app_ssl.crt --from-file=tls.key=security/ca/aas_web_app_ssl.key --type=kubernetes.io/tls


kubectl create secret tls ingress-tls \
--cert ingress.crt --key ingress.key