# manage.py

from flask.cli import FlaskGroup

from project import create_app, db
from project.web.models.user import User

app = create_app()
cli = FlaskGroup(create_app=create_app)


# new
@cli.command("recreate_db")
def recreate_db():
    db.drop_all()
    db.create_all()
    db.session.commit()


@cli.command("seed_db")
def seed_db():
    user = User(username="admin", role="admin")
    user.set_password("admin")
    db.session.add(user)
    user = User(username="iker", role="normal")
    user.set_password("iker")
    db.session.add(user)
    user = User(username="mikele", role="normal")
    user.set_password("mikele")
    db.session.add(user)
    user = User(username="ivan", role="normal")
    user.set_password("ivan")
    db.session.add(user)
    user = User(username="mikel", role="normal")
    user.set_password("mikel")
    db.session.add(user)
    user = User(username="alex", role="normal")
    user.set_password("alex")
    db.session.add(user)
    user = User(username="user", role="normal")
    user.set_password("user")
    db.session.add(user)
    db.session.commit()


if __name__ == "__main__":
    cli()
