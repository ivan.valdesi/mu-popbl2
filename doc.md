# MU-POPBL2-DOC

This document holds the definition and evidences of the Agile methodology followed during the development of the second semester POPBL of the [Data Analysis, Cybersecurity and Cloud Computing](https://www.mondragon.edu/es/master-universitario-analisis-datos-ciberseguridad-computacion-nube) master degree at Mondragon University.

## Introduction

One of the objectives of this project involves the use of an agile methodology for the development of the project. In this section we will show the approach we have chosen to achieve this objective.

For the project we used gitlab.com for version control, the implementation of an agile methodology and the creation of the CI/CD pipelines. We have used this platform during the course, and it is the one that best suits the needs of the project. Regarding the agile methodology, it includes several artifacts that can be mapped to the agile ones.  In the next sections we will explain which parts of the project are stored in gitlab and how it is organized as well as how we adapt what gitlab offers to the project needs.

## Git repository structure

We have used one repository to hold all the parts of the project. This is the web application, the jupyter notebooks for the data intelligence and deep learning part, the security management documents and configuration files of those services that have been considered relevant. The repository is accessible from [this link](https://gitlab.com/ivan.valdesi/mu-popbl2). Following these contents, we have divided the project in these sub-sections.

![](imgs/git_structure.png)

It is worth mentioning at this point that the CI/CD pipeline only involves the contents of the web-app folder. These files will be built, tested, and deployed via the pipeline. However, we found it interesting to have a single repository for the whole project, even if the CI/CD part only affected a part of it. Later, we will explain the branching strategy used to keep this part well differentiated.

## GitLab artifacts

We have defined two milestones for the project.

- Release v1.0: This milestone was set as the deadline to have the first version of the application deployed. Within this milestone are included all changes and bug fixes that have been made on this version after the first release.
- Final release: This milestone is set as the delivery of the final developed product.

To manage the project tasks as well as the application development tasks. The issues the gitlab and a single board have been used for the whole project. The tasks are based on the ones we defined at the start project on the Gantt diagram. We extended that tasks into several issues.

![](imgs/board.png)

As we can see in the image, we have used labels that refer to the subject that involves the issue. This helped us to follow the state of each part of the project. Due to the characteristics of the project it is hard to have all the commits referencing an issue. Despite this, we have tried to close issues by using keywords in the commits. Especially in the development part of the web application. We tried to follow good practices for writing commit messages, focusing on how and why, so that all team members could understand the changes made.

## Branch strategy

As the repository contains the entire project in which we will be working simultaneously five people, it is important to define a branching and git flow strategy that facilitates development. In this section we will explain the decisions we have made.

We have followed a simplified git workflow that makes it easier to deploy. We have three main branches in the repository.

- Dev: All changes to the application will be made in this branch.
- Staging: This branch holds the version of the app that is on the staging cluster.
- Master: This branch holds the version of the app that is on the production cluster.

There are two additional branches (security-management and deep-learning) that are used to develop other sections of the project that are not related to the application development.

![](imgs/branch_strategy.png)

With these strategies we have defined a workflow shown in the image for the development of the application.

- All the changes of the application are committed to the dev branch. On every commit on this branch a pipeline will create the web app and the predictor images and run the tests on them.
- When a new version is ready on dev or just for testing how it works the application in a production-like environment, a merge request is created to the staging. After being reviewed, the content of dev is merged into the staging branch. This will run a pipeline that will run again the build and test and then deploy the new version to the staging cluster.
- When a version in the staging cluster is ready to be deployed to production, a merge request is created from staging to the master branch. After being reviewed, the content of staging is merged into the master branch This will run a pipeline that will create a canary with the new version on the production cluster. This pipeline has two manual stages that makes the final deploy from canary to production when it is considered that the app works correctly.
- The canary deploy to production and the deploy to staging will be rolled back if the health checks fails. We will always be able to roll back the production version of the app manually using kubectl commands.
- This way the master branch will always have the latest stable version of the app that is on production.